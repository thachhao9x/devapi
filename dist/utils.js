"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isWeAPP = exports.endsWith = void 0;
function endsWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
}
exports.endsWith = endsWith;
exports.isWeAPP = () => typeof wx !== 'undefined';
